import {Outlet} from 'solid-app-router';

const StartingWrapper = () => (
  <div class="h-full flex flex-row">
    <div class="hidden lg:flex h-full w-1/2 p-8 lg:p-16 bg-main justify-center items-center">
      <h1 class="text-white font-mono text-4xl md:text-5xl xl:text-7xl">
        Лесна, бърза и ефективна комуникация между стоматолози и зъботехници.
      </h1>
    </div>
    <div class="h-full w-full lg:w-1/2 bg-main-lightest p-8 lg:p-16 flex flex-col justify-center items-center">
      <Outlet />
    </div>
  </div>
);

export default StartingWrapper;
