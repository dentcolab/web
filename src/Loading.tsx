import {onMount} from 'solid-js';
import './styles/output.css';

const Loading = () => {
  let main: HTMLDivElement;
  onMount(() => {
    main.focus();
  });
  return (
    <div
      ref={main}
      tabIndex="-1"
      class="fixed inset-0 z-50 bg-white bg-opacity-50 flex justify-center items-center"
    >
      <p>Test</p>
    </div>
  );
};

export default Loading;
