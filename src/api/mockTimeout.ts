/**
 * An asynchronous sleep function.
 * @param ms Number of miliseconds to delay.
 * @returns A promise that resolves after the specified timeout.
 */
const asyncSleep = (ms: number): Promise<unknown> => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export default asyncSleep;
