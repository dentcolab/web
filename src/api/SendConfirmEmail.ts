import asyncSleep from './mockTimeout';

/**
 *	A call to the confirm email endpoint of the API.
 * @param email {string} The email address to send the email to.
 * @returns An error that the email doesn't exist or true if the call was successful.
 */
const SendConfirmEmailCall = async (email: string) => {
  // mock slow internet speed
  await asyncSleep(2000);
  // simulate API call
  if (Math.random() < 0.5) {
    throw Error('Няма профил с този имейл адрес!');
  } else {
    return true;
  }
};

export default SendConfirmEmailCall;
