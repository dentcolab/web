import {render} from 'solid-js/web';
import {Router, Route, Routes, Navigate} from 'solid-app-router';
import {lazy, Suspense} from 'solid-js';
import {GlobalContextProvider} from './context';
import Loading from './Loading';
import DataWrapper from './api/DataWrapper';
import AccessTokenCall from './api/AccessToken';
const WrongEmail = lazy(() => import('./WrongEmail'));
const LogIn = lazy(() => import('./LogIn'));
const SignUp = lazy(() => import('./SignUp'));
const SendConfirmEmail = lazy(() => import('./SendConfirmEmail'));
const ConfirmEmail = lazy(() => import('./ConfirmEmail'));
const Main = lazy(() => import('./Main'));
const StartingWrapper = lazy(() => import('./StartingWrapper'));

if (module.hot) {
  module.hot.accept();
}

const App = () => {
  return (
    <GlobalContextProvider>
      <Router>
        <Routes>
          <Route
            path="/"
            element={
              <Suspense fallback={<Loading />}>
                <StartingWrapper />
              </Suspense>
            }
          >
            <Route path="login" element={<LogIn />} />
            <Route path="signup" element={<SignUp />} />
            <Route path="confirm-email" element={<SendConfirmEmail />} />
            <Route path="confirm-email/:confirmID" element={<ConfirmEmail />} />
            <Route path="wrong-email" element={<WrongEmail />} />
          </Route>
          <Route
            path="/app"
            element={
              <Suspense fallback={<Loading />}>
                <Main />
              </Suspense>
            }
            data={DataWrapper(AccessTokenCall)}
          >
            <Route path="/" element={<h1 class="text-foreground">Начало</h1>} />
            <Route path="/account" element={<h1 class="text-foreground">Профил</h1>} />
            <Route path="/orders" element={<h1 class="text-foreground">Поръчки</h1>} />
            <Route
              path="/orders/finished"
              element={<h1 class="text-foreground">Завършени Поръчки</h1>}
            />
            <Route
              path="/orders/archived"
              element={<h1 class="text-foreground">Архивирани Поръчки</h1>}
            />
            <Route path="/messages" element={<h1 class="text-foreground">Съобщения</h1>} />
          </Route>
          <Route path="*" element={<Navigate href="/app" />} />
        </Routes>
      </Router>
    </GlobalContextProvider>
  );
};

render(() => <App />, document.getElementById('root'));
