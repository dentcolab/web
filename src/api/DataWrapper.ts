import {createResource, Resource} from 'solid-js';

type DataWrapperType = (fetcher: () => Promise<any>, data?: any) => () => Resource<any>;

const DataWrapper: DataWrapperType = (fetcher: () => Promise<any>, data?: any) => () => {
  if (data === undefined) {
    return createResource(fetcher)[0];
  } else {
    return createResource(data, fetcher)[0];
  }
};
export default DataWrapper;
