import {createContext} from 'solid-js';
import {createStore, Store, SetStoreFunction} from 'solid-js/store';

type GlobalStoreType = {drawer: boolean; authToken: string; id: string; emailToConfirm: string};
type GlobalContextType = [Store<GlobalStoreType>, SetStoreFunction<GlobalStoreType>];

export const GlobalContext = createContext<GlobalContextType>();

export const GlobalContextProvider = props => {
  const [store, setStore] = createStore({drawer: true, authToken: '', id: '', emailToConfirm: ''});
  return (
    <GlobalContext.Provider value={[store, setStore]}>{props.children}</GlobalContext.Provider>
  );
};
