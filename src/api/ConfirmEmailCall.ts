import asyncSleep from './mockTimeout';

/**
 * A call to the confirm email endpoint of the API.
 * @param confirmID {string} The confirmation id.
 * @returns Promise<boolean> - true if the request was successful.
 */
const ConfirmEmailCall = async (confirmID: string) => {
  // mock slow internet speed
  await asyncSleep(2000);
  // simulate API call
  if (Math.random() < 0.5) {
    throw Error();
  } else {
    return true;
  }
};

export default ConfirmEmailCall;
