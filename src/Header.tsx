import {useMatch} from 'solid-app-router';
import {Match, Switch, useContext} from 'solid-js';

import './styles/icofont.min.css';
import {GlobalContext} from './context';

const Header = () => {
  const home = useMatch(() => '/app');
  const account = useMatch(() => '/app/account');
  const orders = useMatch(() => '/app/orders');
  const finishedOrders = useMatch(() => '/app/orders/finished');
  const archivedOrders = useMatch(() => '/app/orders/archived');
  const messages = useMatch(() => '/app/messages');
  const setContext = useContext(GlobalContext)[1];
  const openDrawer = () => {
    setContext('drawer', true);
  };
  return (
    <header class="fixed w-full bg-main flex items-center px-4 py-2 z-20 shadow-xl">
      <div class="justify-self-start flex flex-row items-center h-14">
        <button
          onClick={openDrawer}
          class="lg:hidden h-14 w-14 mr-4 rounded-full bg-main hover:bg-main-dark flex justify-center items-center p-4"
        >
          <span class="icofont-navigation-menu text-white icofont-lg"></span>
        </button>
        <h6 class="h-10 text-2xl text-white font-sans">
          <Switch fallback={<></>}>
            <Match when={home()}>Начало</Match>
            <Match when={account()}>Профил</Match>
            <Match when={orders()}>Поръчки</Match>
            <Match when={finishedOrders()}>Завършени Поръчки</Match>
            <Match when={archivedOrders()}>Архивирани Поръчки</Match>
            <Match when={messages()}>Съобщения</Match>
          </Switch>
        </h6>
      </div>
    </header>
  );
};

export default Header;
