import {useNavigate} from 'solid-app-router';
import './../styles/output.css';
import DrawerButton from './DrawerButton';

// icons
import './../styles/icofont.min.css';

const DrawerContent = () => {
  const navigate = useNavigate();
  return (
    <div class="flex flex-col justify-start">
      <DrawerButton icon={'icofont-ui-user'} onClick={() => navigate('/app/account')}>
        Профил
      </DrawerButton>
      <DrawerButton icon={'icofont-tooth'} onClick={() => navigate('/app/orders')}>
        Поръчки
      </DrawerButton>
      <DrawerButton icon={'icofont-tick-mark'} onClick={() => navigate('/app/orders/finished')}>
        Завършени Поръчки
      </DrawerButton>
      <DrawerButton icon={'icofont-archive'} onClick={() => navigate('/app/orders/archived')}>
        Архивирани Поръчки
      </DrawerButton>
      <DrawerButton icon={'icofont-ui-message'} onClick={() => navigate('/app/messages')}>
        Съобщения
      </DrawerButton>
    </div>
  );
};

export default DrawerContent;
