import {Link} from 'solid-app-router';
import {createResource, createSignal, onCleanup, Show, useContext} from 'solid-js';
import SendConfirmEmailCall from './api/SendConfirmEmail';
import {GlobalContext} from './context';
import Loading from './Loading';
import './styles/output.css';

const SendConfirmEmail = () => {
  const [context, setContext] = useContext(GlobalContext);
  const [email, setEmail] = createSignal(context.emailToConfirm);
  const [submittedEmail, setSubmittedEmail] = createSignal(null);
  const [resource, {refetch}] = createResource(submittedEmail, SendConfirmEmailCall);
  const onSubmit = (e: Event) => {
    e.preventDefault();
    if (submittedEmail() === email()) {
      refetch();
    } else {
      setSubmittedEmail(email());
    }
  };
  onCleanup(() => setContext('emailToConfirm', ''));
  return (
    <>
      <Show when={resource.loading}>
        <Loading />
      </Show>
      <div class="space-y-20 w-full h-full flex flex-col justify-center items-center">
        <h1 class="text-center text-2xl lg:text-4xl text-foreground">
          За да потвърдите имейла си, напишете го в текстовото поле и кликнете на бутона. <br />
          След това, кликнете на линка, който ще получите на посочения от Вас имейл адрес.
          <br />
          Ако не получите такъв имейл до 5 минути, кликнете на бутона за повторно изпращане.
        </h1>
        <form class="w-full space-y-20 flex flex-col items-center" onSubmit={onSubmit}>
          <input
            class="bg-white w-full appearance-none rounded-lg rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
            type="email"
            required
            placeholder="E-mail"
            id="email"
            name="email"
            value={email()}
            onInput={e => setEmail(e.currentTarget.value)}
          />
          <Show when={!resource.loading && resource.error}>
            <h1 class="text-2xl text-foreground text-center mt-10">{resource.error.message}</h1>
          </Show>
          <Show when={!resource.loading && !resource.error && resource()}>
            <h1 class="text-2xl text-foreground text-center mt-10">Имейлът бе изпратен успешно.</h1>
          </Show>
          <button
            type="submit"
            class="w-1/2 text-center text-white text-xl lg:text-3xl bg-main rounded-md p-4 hover:bg-main-dark shadow-lg active:shadow-none"
          >
            Изпращане.
          </button>
        </form>
        <div class="flex flex-col justify-center items-center space-y-5">
          <Link
            href="/wrong-email"
            class="text-main-dark hover:text-main-darker text-center text-2xl"
          >
            Сгрешили сте имейл адреса си при регистрация?
          </Link>
        </div>
      </div>
    </>
  );
};

export default SendConfirmEmail;
