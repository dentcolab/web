import {createMemo, createResource, createSignal, Show} from 'solid-js';
import {Link, useNavigate} from 'solid-app-router';
import './styles/output.css';

import LogInCall from './api/LogIn';
import Loading from './Loading';

/**
 * The log in page component.
 */
export default () => {
  // used to redirect the user to the email confirmation page
  const navigate = useNavigate();
  // signal that is updated each time the user attemts to log in so as to trigger the login resource declared below
  const [submittedForm, setSubmittedForm] = createSignal(null);
  // code and password input field controls
  const [code, setCode] = createSignal('');
  const [password, setPassword] = createSignal('');
  // resource for the log in API endpoint call
  const [login] = createResource(submittedForm, LogInCall);
  const onSubmit = (e: Event) => {
    e.preventDefault();
    setSubmittedForm({code: code(), password: password()});
  };
  createMemo(() => {
    if (!login.loading) {
      if (login.error && login.error.message === 'Имейлът Ви не е потвърден!') {
        navigate('/confirm-email');
      } else if (!login.error && login()) {
        navigate('/app/home');
      }
    }
  });
  return (
    <>
      <Show when={login.loading}>
        <Loading />
      </Show>
      <div class="space-y-20 w-full h-full flex flex-col justify-center items-center">
        <h1 class="text-center text-6xl xl:text-8xl text-foreground font-bold">Вход</h1>
        <form class="w-full flex flex-col items-center space-y-20" onSubmit={onSubmit}>
          <div class="w-full rounded-lg shadow-sm space-y-px">
            <input
              class="bg-white w-full appearance-none rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 rounded-t-lg focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
              type="text"
              required
              placeholder="ЛПК или УИН код"
              value={code()}
              onInput={e => setCode(e.currentTarget.value)}
            />
            <input
              class="bg-white w-full appearance-none rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 rounded-b-lg focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
              type="password"
              autocomplete="current-password"
              required
              placeholder="Парола"
              value={password()}
              onInput={e => setPassword(e.currentTarget.value)}
            />
          </div>
          <Show when={login.error}>
            <h1 class="text-2xl text-foreground text-center mt-10">{login.error.message}</h1>
          </Show>
          <button
            type="submit"
            class="w-1/2 text-center text-white text-3xl bg-main rounded-md p-4 hover:bg-main-dark shadow-lg active:shadow-none"
          >
            Влизане
          </button>
        </form>
        <div class="flex flex-col justify-center items-center space-y-5">
          <Link href="/" class="text-main-dark hover:text-main-darker text-center text-2xl">
            Забравили сте си паролата?
          </Link>
          <Link href="/signup" class="text-main-dark hover:text-main-darker text-center text-2xl">
            Нямате профил?
          </Link>
        </div>
      </div>
    </>
  );
};
