import {Outlet, useData, useNavigate} from 'solid-app-router';
import {createMemo, Resource, Show, useContext} from 'solid-js';
import {AccessTokenResponse} from './api/AccessToken';
import {GlobalContext} from './context';
import './styles/output.css';

import Header from './Header';
import Drawer from './Drawer/Drawer';
import DrawerContent from './Drawer/DrawerContent';

export default () => {
  const EMAIL_ERROR_PREFIX = 'Непотвърден имейл: ';
  const navigate = useNavigate();
  const tokenRequest: Resource<AccessTokenResponse> = useData();
  const setContext = useContext(GlobalContext)[1];
  createMemo(() => {
    if (!tokenRequest.error) {
      setContext(tokenRequest());
    } else if (tokenRequest.error.message.startsWith(EMAIL_ERROR_PREFIX)) {
      setContext('emailToConfirm', tokenRequest.error.message.slice(EMAIL_ERROR_PREFIX.length, -1));
      navigate('/confirm-email');
    } else {
      navigate('/login');
    }
  });
  return (
    <Show when={!tokenRequest.error || !tokenRequest.error.message.startsWith(EMAIL_ERROR_PREFIX)}>
      <Header />
      <Drawer>
        <DrawerContent />
      </Drawer>
      <main class="w-full h-full pt-18 lg:pl-60 bg-main-lightest">
        <Outlet />
      </main>
    </Show>
  );
};
