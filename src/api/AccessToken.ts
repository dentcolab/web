export type AccessTokenResponse = {
  id: string;
  authToken: string;
};

/**
 * Requests a JWT access token with the refresh token stored in HttpOnly cookie. Returns the token or throws an error if there is no refresh token.
 * @returns {AccessTokenResponse} The user ID and the JWT access token
 */
const AccessTokenCall = async () => {
  if (Math.random() < 0.1) {
    if (Math.random() < 0.6) {
      throw Error('Непотвърден имейл: test@gmail.com!');
    } else {
      throw Error('Сървърна грешка!');
    }
  } else {
    // return sample token
    return {
      id: 'example',
      authToken:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
    };
  }
};

export default AccessTokenCall;
