import {createEffect, createMemo, createResource, createSignal, onCleanup, Show} from 'solid-js';
import {Navigate, useNavigate, useParams} from 'solid-app-router';
import ConfirmEmailCall from './api/ConfirmEmailCall';

const ConfirmEmail = () => {
  const navigate = useNavigate();
  const confirmID = useParams().confirmID;
  const resource = createResource(confirmID, ConfirmEmailCall)[0];
  const [timer, setTimer] = createSignal(3);
  let interval;
  createMemo(() => {
    if (!resource.loading) {
      interval = setInterval(() => setTimer(prev => prev - 1), 1000);
    }
  });
  createEffect(() => {
    if (!resource.loading && timer() <= 0) {
      if (interval) {
        clearInterval(interval);
      }
      if (!resource.error) {
        navigate('/app');
      } else {
        // error
        navigate('/confirm-email');
      }
    }
  });
  onCleanup(() => {
    if (interval) {
      clearInterval(interval);
    }
  });
  return (
    <div class="w-full h-full flex flex-col justify-center items-center">
      <h1 class="text-center text-2xl lg:text-4xl text-foreground">
        <Show when={resource.loading}>Потвърждаване. Моля изчакайте...</Show>
        <Show when={!resource.loading && resource.error}>
          Грешка при потвърждаването. Пренасочване след {timer()}
        </Show>
        <Show when={!resource.loading && !resource.error}>
          Успешно потвърдихте имейла си. Пренасочване след {timer()}
        </Show>
      </h1>
    </div>
  );
};

export default ConfirmEmail;
