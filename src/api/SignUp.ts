import asyncSleep from './mockTimeout';

/**
 *	A call to the sign up endpoint of the application API
 * @param user An object that contains the job position, the email, the identification code, and the password for the new account.
 * @returns One of the possible API errors or true if the call was successful
 */
const SignUpCall = async (user: {email: string; code: string; password: string}) => {
  // mock slow internet speed
  await asyncSleep(2000);
  // simulate API call with all possible errors and empty response
  if (Math.random() < 0.1) {
    const rand = Math.random();
    if (rand < 0.4) {
      throw Error('Вече съществува акаунт с този код!');
    } else if (rand < 0.8) {
      throw Error('Имейлът вече се използва за друг акаунт!');
    } else {
      throw Error('Сървърна грешка!');
    }
  } else {
    return true;
  }
};

export default SignUpCall;
