import {createSignal, For, onCleanup} from 'solid-js';
import '../styles/output.css';

const DrawerButton = props => {
  const [ripples, setRipples] = createSignal([]);
  const onClick = e => {
    const rippleContainer = e.currentTarget;
    const size = rippleContainer.offsetWidth;
    const pos = rippleContainer.getBoundingClientRect();
    const x = e.pageX - pos.x - size / 2;
    const y = e.pageY - pos.y - size / 2;
    const spanStyles = {top: y + 'px', left: x + 'px', height: size + 'px', width: size + 'px'};
    setRipples(ripples => [
      ...ripples,
      {styles: spanStyles, timeout: setTimeout(() => setRipples(r => r.slice(1)), 750)},
    ]);
    props.onClick && props.onClick(e);
  };
  onCleanup(() => {
    ripples().forEach(r => clearTimeout(r.timeout));
    setRipples([]);
  });
  return (
    <button
      class="w-full p-4 relative text-white text-2xl text-left font-sans overflow-hidden hover:bg-main-darker flex flex-row items-center"
      onClick={onClick}
    >
      <span class={'w-6 h-6 text-2xl mr-4' + ' ' + props.icon}></span>
      {props.children}
      <div class="absolute inset-0">
        <For each={ripples()}>{ripple => <span class="ripple" style={ripple.styles}></span>}</For>
      </div>
    </button>
  );
};

export default DrawerButton;
