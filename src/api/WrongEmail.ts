import asyncSleep from './mockTimeout';

/**
 *	A call to the wrong email endpoint of the API.
 * @param data {{code: string, password: string, email: string}} The email address to send the email to.
 * @returns An error that the code doesn't exist, an error that the password is incorrect, or true if the call was successful.
 */
const WrongEmailCall = async (data: {code: string; password: string; email: string}) => {
  // mock slow internet speed
  await asyncSleep(2000);
  // simulate API call
  if (Math.random() < 0.1) {
    if (Math.random() < 0.5) {
      throw Error('Няма профил с този код!');
    } else {
      throw Error('Грешна парола!');
    }
  } else {
    return true;
  }
};

export default WrongEmailCall;
