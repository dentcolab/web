/* eslint @typescript-eslint/no-var-requires: "off" */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.tsx',
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              babelrc: false,
              configFile: false,
              presets: [
                ['@babel/preset-env', {corejs: '3.16.1', useBuiltIns: 'usage'}],
                'babel-preset-solid',
              ],
              sourceType: 'module',
            },
          },
          'ts-loader',
        ],
      },
      {test: /\.(sass|scss)$/, use: ['style-loader', 'css-loader', 'sass-loader']},
      {test: /\.css$/, use: ['style-loader', 'css-loader']},
      {
        test: /\.(jpg|png|svg)$/,
        loader: 'url-loader',
        options: {
          name: '[path][name].[hash].[ext]',
        },
      },
    ],
  },
  resolve: {
    enforceExtension: false,
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  },
  output: {
    path: path.resolve(__dirname, './public'),
    publicPath: '/',
    filename: 'index.bundle.js',
  },
  devServer: {
    contentBase: './public',
    hot: true,
    port: 8080,
    historyApiFallback: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],
};
