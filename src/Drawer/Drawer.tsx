import {createComputed, createSignal, Show, useContext} from 'solid-js';
import {GlobalContext} from './../context';
import './../styles/output.css';

import './../styles/Drawer.scss';

const Drawer = props => {
  const [context, setContext] = useContext(GlobalContext);
  // signal that delays the disappearance of elements in order for the animations to finish
  // should be synced with the global drawer context property
  const [drawer, setDrawer] = createSignal(true);
  createComputed(() => {
    if (context.drawer) {
      setDrawer(true);
    }
  });
  return (
    <>
      <Show when={drawer()}>
        <div
          class={`lg:hidden fixed inset-0 bg-black bg-opacity-80 z-30 ${
            context.drawer ? 'drawer-background-open' : 'drawer-background-close'
          }`}
          onClick={() => {
            setContext('drawer', false);
            setTimeout(() => setDrawer(false), 150);
          }}
        ></div>
        <div
          class={`${
            context.drawer ? 'drawer-open' : 'drawer-close'
          } lg:hidden fixed inset-y-0 w-60 bg-main-dark z-40`}
        >
          {props.children}
        </div>
      </Show>
      <div class="hidden lg:inline-block fixed inset-y-0 w-60 bg-main-dark z-10 shadow-xl">
        <div class="w-full h-18"></div>
        {props.children}
      </div>
    </>
  );
};

export default Drawer;
