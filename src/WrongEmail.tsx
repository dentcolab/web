import {createMemo, createResource, createSignal, onCleanup, Show, useContext} from 'solid-js';
import {Link, useNavigate} from 'solid-app-router';
import WrongEmailCall from './api/WrongEmail';
import Loading from './Loading';
import {GlobalContext} from './context';

const WrongEmail = () => {
  const navigate = useNavigate();
  const setContext = useContext(GlobalContext)[1];
  const [submittedForm, setSubmittedForm] =
    createSignal<{code: string; password: string; email: string}>(null);
  const [code, setCode] = createSignal('');
  const [password, setPassword] = createSignal('');
  const [email, setEmail] = createSignal('');
  const [resource] = createResource(submittedForm, WrongEmailCall);
  const onSubmit = (e: Event) => {
    e.preventDefault();
    setSubmittedForm({code: code(), password: password(), email: email()});
  };
  let timeout;
  createMemo(() => {
    if (!resource.loading && !resource.error && resource()) {
      setContext('emailToConfirm', submittedForm().email);
      timeout = setTimeout(() => navigate('/confirm-email'), 3000);
    }
  });
  onCleanup(() => {
    if (timeout) {
      clearTimeout(timeout);
    }
  });
  return (
    <>
      <Show when={resource.loading}>
        <Loading />
      </Show>
      <div class="space-y-20 flex flex-col justify-center items-center">
        <h1 class="text-center text-2xl lg:text-4xl text-foreground">
          За да смените имейла си, напишете кода и паролата на профила си в съответните текстови
          полета, както и новия си имейл в следващото поле.
        </h1>
        <form class="w-full flex flex-col items-center space-y-20" onSubmit={onSubmit}>
          <div class="rounded-md shadow-sm space-y-px w-full">
            <input
              class="bg-white w-full appearance-none rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 rounded-t-lg focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
              type="text"
              required
              placeholder="ЛПК или УИН код"
              value={code()}
              onInput={e => setCode(e.currentTarget.value)}
            />
            <input
              class="bg-white w-full appearance-none rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 rounded-b-lg focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
              type="password"
              autocomplete="current-password"
              required
              placeholder="Парола"
              value={password()}
              onInput={e => setPassword(e.currentTarget.value)}
            />
          </div>
          <input
            class="bg-white w-full appearance-none rounded-lg rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
            type="email"
            required
            placeholder="E-mail"
            id="email"
            name="email"
            value={email()}
            onInput={e => setEmail(e.currentTarget.value)}
          />
          <Show when={!resource.loading && resource.error}>
            <h1 class="text-2xl text-foreground text-center mt-10">{resource.error.message}</h1>
          </Show>
          <Show
            when={!resource.loading && !resource.error && resource()}
            fallback={
              <button
                type="submit"
                class="w-1/2 text-center text-white text-xl lg:text-3xl bg-main rounded-md p-4 hover:bg-main-dark shadow-lg active:shadow-none"
              >
                Изпращане.
              </button>
            }
          >
            <h1 class="text-2xl text-foreground text-center mt-10">
              Успешно променихте имейла си. Пренасочване...
            </h1>
          </Show>
        </form>
        <div class="flex flex-col justify-center items-center space-y-5">
          <Link
            href="/confirm-email"
            class="text-main-dark hover:text-main-darker text-center text-2xl"
          >
            Спомнихте си имейла?
          </Link>
        </div>
      </div>
    </>
  );
};

export default WrongEmail;
