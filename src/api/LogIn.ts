import asyncSleep from './mockTimeout';

/**
 *	A call to the log in endpoint of the application API
 * @param user An object that contains the identification code and password of a user.
 * @returns One of the possible API errors or true if the call was successful
 */
const LogInCall = async (user: {code: string; password: string}) => {
  // mock slow internet speed
  await asyncSleep(2000);
  // simulate API call with all possible errors and empty response
  if (Math.random() < 0.1) {
    const rand = Math.random();
    if (rand < 0.2) {
      throw Error('Имейлът Ви не е потвърден!');
    }
    if (rand < 0.4) {
      throw Error('Грешен ЛПК или УИН код!');
    } else if (rand < 0.8) {
      throw Error('Грешна парола!');
    } else {
      throw Error('Сървърна грешка!');
    }
  } else {
    return true;
  }
};

export default LogInCall;
