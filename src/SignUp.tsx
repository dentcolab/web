import {createMemo, createResource, createSignal, Show, useContext} from 'solid-js';
import {createStore} from 'solid-js/store';
import {Link, useNavigate} from 'solid-app-router';
import './styles/output.css';
import SignUpCall from './api/SignUp';
import Loading from './Loading';
import {GlobalContext} from './context';

/**
 * The sign up page component.
 */
export default () => {
  const navigate = useNavigate();
  const setContext = useContext(GlobalContext)[1];
  // signal that is updated each time the user submits the form
  const [submittedForm, setSubmittedForm] = createSignal(null);
  // form controls
  const [formValues, setFormValues] = createStore({
    profession: '',
    email: '',
    code: '',
    password: '',
    confirmPassword: '',
  });
  const [signup] = createResource(submittedForm, SignUpCall);
  const onSubmit = (e: Event) => {
    e.preventDefault();
    setSubmittedForm({...formValues});
  };
  createMemo(() => {
    if (!signup.loading && !signup.error && signup()) {
      setContext('emailToConfirm', submittedForm().email);
      navigate('/confirm-email');
    }
  });
  return (
    <>
      <Show when={signup.loading}>
        <Loading />
      </Show>
      <div class="space-y-20 flex flex-col justify-center items-center">
        <h1 class="text-center text-6xl xl:text-8xl font-bold text-foreground">Регистрация</h1>
        <div class="inline-block relative w-4/5">
          <select
            value={formValues.profession}
            onInput={e => setFormValues('profession', e.currentTarget.value)}
            class="appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 rounded px-4 py-3 leading-tight focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
          >
            <option value="" disabled>
              -- изберете професия --
            </option>
            <option value="dentist">Стоматолог</option>
            <option value="dt">Зъботехник</option>
          </select>
          <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg
              class="fill-current h-4 w-4"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
            </svg>
          </div>
        </div>
        <Show
          when={formValues.profession !== ''}
          fallback={
            <h1 class="text-center text-2xl xl:text-4xl text-foreground">
              Изберете професията си от менюто по-горе.
            </h1>
          }
        >
          <form class="w-full flex flex-col items-center space-y-20" onSubmit={onSubmit}>
            <div class="w-full flex flex-col items-center">
              <div class="shadow-sm space-y-px">
                <input
                  class={
                    'bg-white w-full appearance-none rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent rounded-t-lg'
                  }
                  type="email"
                  required
                  placeholder="E-mail"
                  id="email"
                  name="email"
                  value={formValues.email}
                  onInput={e => setFormValues('email', e.currentTarget.value)}
                />
                <input
                  class="bg-white w-full appearance-none rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
                  type="text"
                  required
                  placeholder={`${formValues.profession === 'dentist' ? 'ЛПК' : 'УИН'} код`}
                  id="code"
                  name="code"
                  value={formValues.code}
                  onInput={e => setFormValues('code', e.currentTarget.value)}
                />
                <input
                  class="bg-white w-full appearance-none rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
                  type="password"
                  required
                  placeholder="Парола"
                  id="password"
                  name="password"
                  value={formValues.password}
                  onInput={e => setFormValues('password', e.currentTarget.value)}
                />
                <input
                  class="bg-white w-full appearance-none rounded-none px-4 py-3 border border-gray-400 hover:border-gray-500 placeholder-gray-600 rounded-b-lg focus:outline-none focus:ring-2 focus:ring-main-dark focus:border-transparent"
                  type="password"
                  required
                  placeholder="Потвърдете парола"
                  id="confirmPassword"
                  name="confirmPassword"
                  value={formValues.confirmPassword}
                  onInput={e => setFormValues('confirmPassword', e.currentTarget.value)}
                />
              </div>
              <Show when={signup.error}>
                <h1 class="text-2xl text-foreground text-center mt-10">{signup.error.message}</h1>
              </Show>
            </div>
            <button
              type="submit"
              class="w-1/2 text-center text-white text-3xl bg-main rounded-md p-4 hover:bg-main-dark shadow-lg active:shadow-none"
            >
              Създаване
            </button>
          </form>
        </Show>
        <div class="flex flex-col justify-center items-center space-y-5">
          <Link href="/login" class="text-main-dark hover:text-main-darker text-center text-2xl">
            Имате направен профил?
          </Link>
        </div>
      </div>
    </>
  );
};
